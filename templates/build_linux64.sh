#!/bin/sh

# Call general script
# Argument 1 - Build folder
# Argument 2 - Resource global file
# Argument 3 - Resource platform file
# Argument 4 - Linux icon (referenced in the desktop file)
# Argument 5 - Executable name
# Argument 6 - AppImage desktop file

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")

# Absolute path this script is in, thus /home/user/bin
PATHS=$(dirname "$SCRIPT")

arg1="$PATHS"
arg2="$PATHS/resources_global.qrc"
arg3="$PATHS/resources_linux.qrc"
arg4="$PATHS/resources/appIcon.png"
arg5="AppName"
arg6="$PATHS/resources/app.desktop"

/surf/build/build_surf_linux64.sh "$arg1" "$arg2" "$arg3" "$arg4" "$arg5" "$arg6"
