#!/bin/sh

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")

# Absolute path this script is in, thus /home/user/bin
PATHS=$(dirname "$SCRIPT")

# User defined directories
SURF_SRC="$PATHS/../src"

# Compilation directory
PATHB=$1

# Create example and build directory
mkdir -p $PATHB/obj

# Absolute path to the global resource file
RESOURCE=$(readlink -f "$2")

# Absolute path the resource file is in
PATHR=$(dirname "$RESOURCE")

# Copy global resources file
cp $2 "$PATHB/obj/resources_global.qrc"

# Substitute in resource file the tag #CURRENT_DIR# for the path where the resource file is
sed -i "s|#CURRENT_DIR#|${PATHR}|g" "$PATHB/obj/resources_global.qrc"

# Absolute path to the global resource file
RESOURCE=$(readlink -f "$3")

# Absolute path the resource file is in
PATHR=$(dirname "$RESOURCE")

# Copy global resources file
cp $3 "$PATHB/obj/resources.qrc"

# Substitute in resource file the tag #CURRENT_DIR# for the path where the resource file is
sed -i "s|#CURRENT_DIR#|${PATHR}|g" "$PATHB/obj/resources.qrc"

# Set binary name (without extension)
name=$(echo $4 | cut -f 1 -d '.')
echo $name >  "$SURF_SRC/binary_name"

# Build path
cd "$PATHB/obj/"

# QMake
qmake-qt5 "$SURF_SRC/surf.pro" "CONFIG+=staticlib"  -spec linux-g++

# Make
make

# Execute binary file
"$PATHB/obj/$4"
