model BouncingBall "Bouncing ball model"
    // Import packages
    import SI = Modelica.SIunits;
    import Cte = Modelica.Constants;
    // Parameters
    parameter Real c(min=0,max=1) = 0.5 "Coefficient of restitution (-)";
    parameter SI.Length h0(min=0) = 1.0 "Initial height (m)";
    parameter SI.Velocity v0 = 0 "Initial velocity (m/s)";
    parameter SI.Length hmin(min=0) = 1e-4 "Minimum height (m)";
    parameter SI.Velocity vmin(min=0) = 1e-4 "Minimum velocity (m/s)";
    // Variables
    SI.Length h "Height";
    SI.Velocity v "Vertical velocity";
    // Initial conditions
    initial equation
      h = h0;
      v = v0;
    // Model equations
    equation
      der(h) = v;
      der(v) = if h>=hmin and vmin>=vmin then -Cte.g_n else 0;
      when h <= 0 then
        reinit(v, -c * pre(v));
      end when;
end BouncingBall;