#!/bin/sh

# Absolute path this script is in, thus /home/user/bin
PATHS=$(cd "$(dirname "$0")"; pwd)

# Binary name
rm "$PATHS/../../src/binary_name"
echo "TankSim" >> "$PATHS/../../src/binary_name"
# Copy icon
cp "$PATHS/resources/appico.ico" "$PATHS/../../Debug/"

# Copy global resource file
cp "$PATHS/resources_global.qrc" "$PATHS/../../Debug/resources_global.qrc"

# Copy resource file
cp "$PATHS/resources_mac.qrc" "$PATHS/../../Debug/resources.qrc"

# Substitute in resource file the tag #CURRENT_DIR# for the path where the resource file is
sed -i -e "s|#CURRENT_DIR#|${PATHS}|g" "$PATHS/../../Debug/resources_global.qrc"
sed -i -e "s|#CURRENT_DIR#|${PATHS}|g" "$PATHS/../../Debug/resources.qrc"
