/*
*    This file is part of Surf
*
*    Surf - Simulation of FMUs
*    Copyright (C) 2017 - Javier Bonilla
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "svgviewer.h"
#include "ui_svgviewer.h"

#include <QFile>
#include <QClipboard>
#include <QFileDialog>
#include <QSvgRenderer>
#include <QLayout>
#include <QXmlInputSource>

#include "commonapp.h"
#include "common.h"
#include "commonsimint.h"
#include "videoconf.h"

const QString sbWhite = "QWidget{background-color: white;}";
const QString iHide = ":/icons/hint.svg";
const QString iShow = ":/icons/visibility.svg";
const QString iConf_normal  = ":/icons/configure-shortcuts.svg";
const QString iConf_warning = ":/icons/configure-shortcuts-red.svg";

SVGViewer::SVGViewer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SVGViewer)
{
    ui->setupUi(this);

    view = new QSvgWidget(ui->widget);

    setStyleSheet(sbWhite);
    view->setStyleSheet(sbWhite);
    set_svg_icon = false;

    cmSVG.addAction(ui->actionClipboard);
    cmSVG.addAction(ui->actionSave);
    cmSVG.addAction(ui->actionSave_video);
    cmSVG.addAction(ui->actionButton_bar);

    ui->bPause->setEnabled(false);
}

SVGViewer::~SVGViewer()
{
    delete view;
    delete ui;
}

void SVGViewer::loadFile(QString filename)
{
    if (!set_svg_icon)
    {
        QSvgRenderer render;

        // Load XML file
        XML_to_QDom(filename,doc);

        // Render SVG to get default size
        render.load(filename);

        // Aspect ratio
        aspect_ratio = (double)render.defaultSize().width()/(double)render.defaultSize().height();

        // Icon set
        set_svg_icon = true;

        // Install filter to resizing the SVG
        ui->widget->installEventFilter(this);

        // Set size
        SVG_setSize(view,aspect_ratio,ui->widget->width(),ui->widget->height());

        // Button bar action icon
        barIcon();
    }
}

void SVGViewer::configureAnimation(int f, double m)
{
    fps = f;
    multiplier = m;
}

QDomDocument SVGViewer::getDoc() const
{
    return doc;
}

void SVGViewer::setDoc(const QDomDocument &value)
{
    doc = value;
}

void SVGViewer::barIcon()
{
    ui->actionButton_bar->setIcon(ui->fSimulation->isVisible() ? QIcon(iHide) : QIcon(iShow));
}

bool SVGViewer::getSimPanelEnabled() const
{
    return simPanelEnabled;
}

void SVGViewer::setSimPanelEnabled(bool value)
{
    simPanelEnabled = value;
    ui->actionButton_bar->setEnabled(simPanelEnabled);
    if (ui->fSimulation->isVisible())
        ui->fSimulation->setVisible(simPanelEnabled);
}

void SVGViewer::refresh()
{
    SVG_refresh(doc,view,ui->widget->width(),ui->widget->height(),aspect_ratio);
}

bool SVGViewer::eventFilter(QObject *obj, QEvent *event)
{
    if (obj == ui->widget && event->type() == QEvent::Resize)
            SVG_setSize(view,aspect_ratio,ui->widget->width(),ui->widget->height());

    // To process other events
    return false;
}

void SVGViewer::on_actionClipboard_triggered()
{
    imageToImg(getImage(view),true,sEmpty);
}

void SVGViewer::on_actionSave_triggered()
{
    saveSVGimage(this,doc,view);
}

void SVGViewer::on_SVGViewer_customContextMenuRequested(const QPoint &pos)
{
    cmSVG.exec(mapToGlobal(pos));
}

void SVGViewer::setState()
{
    bool enabled = !time_output.isEmpty() && !steady;

    if (enabled)
    {
        VideoConf video;
        double    sLength = time_output.last() - time_output.first();

        // Set default data
        video.setData(sLength,time_output.count(),fps);
        video.setSaveToFile(false);
        video.setMultiplier(multiplier);

        validVideoConfig = video.validState();

        // Hint to configure video settings
        ui->bConfigure->setIcon(QIcon(validVideoConfig ? iConf_normal : iConf_warning));
    }
    else
    {
        validVideoConfig = true;
        ui->bConfigure->setIcon(QIcon(iConf_normal));
    }

    ui->actionSave_video->setEnabled(enabled);
    setPlay(enabled);
}

void SVGViewer::updateInfo(experiment e, opt_model m, QList<QList<double> > vo, QList<double> to, int p, bool s)
{
    exp           = e;
    mo            = m;
    values_output = vo;
    time_output   = to;
    position      = p;
    steady        = s;

    setState();
}

void SVGViewer::evaluateSvgDiagram()
{
    QDomDocument docd = getDoc();

    for(int i=0;i<exp.getLinks().count();i++)
    {
        link l = exp.getLinks()[i];

        if (l.getType() == LINK_ASSIGN)
            evaluateLinkAssign(mo,values_output,docd,l,position);
        else if (l.getType() == LINK_TEXT)
            evaluateLinkText(mo,values_output,docd,l,position);
    }
    setDoc(docd);
    refresh();
}

void SVGViewer::on_actionSave_video_triggered()
{
    saveSVGVideo(view,doc,exp,mo,time_output,values_output,position,multiplier,fps);
}

void SVGViewer::on_actionButton_bar_triggered()
{
    ui->fSimulation->isVisible() ? ui->fSimulation->hide() : ui->fSimulation->show();
    barIcon();
}

void SVGViewer::setBackward(bool value){ui->bBackward->setEnabled(value); ui->bBegin->setEnabled(value);}
void SVGViewer::setForward(bool value){ui->bForward->setEnabled(value); ui->bEnd->setEnabled(value);}
void SVGViewer::setSimBar(bool value){ui->sSim->setEnabled(value);}
void SVGViewer::setTSim(QString value){ui->lTsim->setText(value);}
void SVGViewer::setsSim(int value){if (ui->sSim->value()!=value) ui->sSim->setValue(value);}
void SVGViewer::setsSimMin(int value){ui->sSim->setMinimum(value);}
void SVGViewer::setsSimMax(int value){ui->sSim->setMaximum(value);}
void SVGViewer::setPlay(bool value){ui->bSimulate->setEnabled(value && validVideoConfig); ui->bConfigure->setEnabled(value);}
void SVGViewer::setPause(bool value){ui->bPause->setEnabled(value);}

void SVGViewer::on_sSim_valueChanged(int value)
{
    sSimValue(value);
}

void SVGViewer::on_bForward_clicked()
{
    forward();
}

void SVGViewer::on_bBackward_clicked()
{
    backward();
}

void SVGViewer::on_bEnd_clicked()
{
    simEnd();
}

void SVGViewer::on_bBegin_clicked()
{
    simBegin();
}

void SVGViewer::delay(int ms)
{
    QTime dieTime= QTime::currentTime().addMSecs(ms);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents();
}

void SVGViewer::on_bConfigure_clicked()
{
    VideoConf video;
    double    sLength = time_output.last() - time_output.first();

    // Set default data
    video.setData(sLength,time_output.count(),fps);
    video.setSaveToFile(false);
    video.setMultiplier(multiplier);

    // Open dialog
    if (video.exec() == QDialog::Accepted)
    {
        // Get multiplier
        multiplier = video.getMultiplier();

        // Get FPS
        fps = video.getFPS();
    }

    setState();
}

void SVGViewer::printRealTime(QString s) //double t)
{
    ui->lTreal->setText(s);//"Real-time: " + QString::number(t/1000) + " s");
}

void SVGViewer::on_bSimulate_clicked()
{
    // WARNING: Bug (wrong video time) when numbre of frames is a real number
    // WARNING: the code must be shared with the code in save sgv to video file

    QList<int>     frameNum;
    QHash<int,int> frameCount;
    int            sFrames = time_output.count();                      // Number of frames
    double         sLength = time_output.last() - time_output.first(); // Simulation total time
    double         tLength = sLength * multiplier;                     // Video length

    // Play signal
    play();
    playing = true;

    // Current position or beggining position
    position = ui->sSim->value() == time_output.count()-1 ? 0 : ui->sSim->value();

    // frameNum   = 0, 1, 2, 3 (seconds)
    // frameCount - number of frames in eaach second
    for(int i=0;i<sFrames;i++)
    {
        double t  = time_output[i]-time_output.first();
        double tm = t*multiplier;
        int    n  = (int)tm;

        frameNum.append(n);
        frameCount[n] = frameCount[n]+1;
    }

    // Real time
    QElapsedTimer timer;
    timer.start();

    // For each second i = 0, 1, 2, 3
    int iMax = ceil(tLength);
    for(int i=0; i<iMax && position<time_output.count() && playing;i++)
    {
        // Each second should show FPS frames or less
        int    count     = frameCount[i];                    // Frame count for second i
        double fpsi      = tLength>=1 ? fps  : fps*tLength;  // Total frames in second i
        double increment = count / fpsi;                     // Increment in frames (double)
        double pos       = position;                         // Position (double)

        // For each frame in second i
        for(int j=1;j<=fpsi && increment>0 && position<time_output.count() && playing;j++)
        {
            // New position
            pos += increment;
            // Set position (last iteration)
            position = i==iMax-1 && j+1>fpsi ? time_output.count()-1 : pos;
            setsSim(position);
            // Waiting time
            int msec = 1000*i + 1000*j/fps - timer.elapsed();
            if (msec>0){
                delay(msec);
                ui->lFPS->clear();
                ui->lFPS->setToolTip(sEmpty);
            }
            else{
                ui->lFPS->setText("High FPS");
                ui->lFPS->setToolTip("Too high FPS for current animation configuration");
            }
            // Print real time
            printRealTime("Real time: " + QString::number(timer.elapsed()/1000.0) + " s");
            //printRealTime(QString::number(1000*i + 1000*j/fps) + " - " + QString::number(fps) + " - " + QString::number(timer.elapsed()) + " = " + QString::number(msec));
            // Process events
            QCoreApplication::processEvents();
        }

        // Drecrease video length in 1 second
        tLength -=1;
    }

//    // Set frames for each second
//    int    iIni = (time_output[position]-time_output.first())*multiplier;
//    int    i;
//    double nf;
//    double inc;
//    double acu;

//    qDebug() << "iIni = " << iIni;
//    qDebug() << "jIni = " << jIni;


//    QElapsedTimer timer;
//    timer.start();

//    for(i=iIni;i<nFrames/fps && playing;i++)
//    {
//        int j = jIni;
//        nf  = frameCount[i];
//        inc = nf/fps;
//        acu = position+inc;
//        while(inc>0 && j<fps && playing)
//        {
//            setsSim(position);
//            QCoreApplication::processEvents();
//            // wait time
//            int msec = 1000*(i-iIni) + 1000*(j+1)/fps - timer.elapsed();
//            if (msec>0) delay(msec);
//            printRealTime(timer.elapsed());
//            //
//            acu+=inc;
//            position = acu;
//            j++;
//        }
//        jIni = 0;
//    }

//    if (playing)
//    {
//        // Remainig time less than a second
//        nf  = frameCount[i];
//        inc = nf/nFrames;
//        acu = position+inc;
//        i   = i*fps;
//        while(i<nFrames && playing)
//        {
//            if(i==nFrames-1) position = nf;
//            setsSim(position);
//            QCoreApplication::processEvents();
//            // wait time
//            int msec = 1000*(i+1)/fps - timer.elapsed();
//            if (msec>0) delay(msec);
//            printRealTime(timer.elapsed());
//            //
//            acu+=inc;
//            position = acu;
//            i++;
//        }
//    }

    // Finished signal
    finished();
}

void SVGViewer::on_bPause_clicked()
{
    // Finished signal
    playing = false;
    finished();
}
