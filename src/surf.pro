#-------------------------------------------------
#
# Project created by QtCreator 2017-04-20T09:05:20
#
#-------------------------------------------------

#-------------------------------------------------------------------------#
# Common code paths                                                       #
#-------------------------------------------------------------------------#
COMMON_SRC    = $${PWD}/FMI_common/common
COMMONGUI_SRC = $${PWD}/FMI_common/common_gui

#-------------------------------------------------------------------------#
# Libraries' sources and paths                                            #
#-------------------------------------------------------------------------#
unix:!macos{
    QUAZIP_SRC = /usr/include/quazip5/
    FMIPP_LIB  = $${PWD}/../../ThirdParty/build/fmipp-build
}

win32-g++{
    DEFINES      += QUAZIP_STATIC
    QUAZIP_SRC    = $${PWD}/../../ThirdParty/src/quazip/quazip
    QUAZIP_LIB    = $${PWD}/../../ThirdParty/build-win/quazip-build
    FMIPP_LIB     = $${PWD}/../../ThirdParty/build-win/fmipp-build
    SUNDIALS_SRC  = $${PWD}/../../ThirdParty/build-win/sundials-build/include/
    SUNDIALS_LIB  = $${PWD}/../../ThirdParty/build-win/sundials-build
}

macos{
    DEFINES      += QUAZIP_STATIC
    QUAZIP_SRC    = $${PWD}/../../ThirdParty/src/quazip/quazip
    QUAZIP_LIB    = $${PWD}/../../ThirdParty/build/quazip-build
    FMIPP_LIB     = $${PWD}/../../ThirdParty/build/fmipp-build
    SUNDIALS_SRC  = $${PWD}/../../ThirdParty/build/sundials-build/include/
    SUNDIALS_LIB  = $${PWD}/../../ThirdParty/build/sundials-build
}

FMIPP_SRC  = $${PWD}/../../ThirdParty/src/fmipp-code
#-------------------------------------------------------------------------#

QT          += core gui xml charts printsupport svg script
RESOURCES    = resources_global.qrc resources.qrc internal.qrc
BINARY_NAME  = "$$cat(binary_name)"
TARGET       = $${BINARY_NAME}
TEMPLATE     = app

qtHaveModule(opengl): QT += opengl

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS USE_SUNDIALS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# ---------------- #
# Icon for Windows #
# ---------------- #
win32-g++: RC_ICONS += appico.ico

#------------------------------- #
# For cross-compilation of FMI++ #
#------------------------------- #
win32-g++: DEFINES += MINGW

SOURCES += main.cpp\
           mainwindow.cpp \
           commonsimint.cpp \
           svgviewer.cpp \
           about.cpp \
           simlog.cpp \
           resultwin.cpp \
           $${COMMON_SRC}/opt_project.cpp \
           $${COMMON_SRC}/simulation.cpp \
           $${COMMON_SRC}/common.cpp \
           $${COMMON_SRC}/formats/libcsv.c \
           $${COMMON_SRC}/formats/read_csv.c \
           $${COMMON_SRC}/formats/read_matlab4.c \
           $${COMMON_SRC}/formats/write_matlab4.c \
           $${COMMON_SRC}/opt_problem.cpp \
           $${COMMONGUI_SRC}/commonapp.cpp \
           $${COMMONGUI_SRC}/brushconf.cpp \
           $${COMMONGUI_SRC}/graphconfig.cpp \
           $${COMMONGUI_SRC}/penconfig.cpp \
           $${COMMONGUI_SRC}/seriesconfig.cpp \
           $${COMMONGUI_SRC}/integrator.cpp \
           $${COMMONGUI_SRC}/outputs.cpp \
           $${COMMONGUI_SRC}/plotgraphs.cpp \
           $${COMMONGUI_SRC}/qtabwidgetext.cpp \
           $${COMMONGUI_SRC}/videoconf.cpp

HEADERS  += mainwindow.h \
            commonsimint.h \
            svgviewer.h \
            about.h \
            simlog.h \
            resultwin.h \
            $${COMMON_SRC}/simulation.h \
            $${COMMON_SRC}/common.h \
            $${COMMON_SRC}/formats/libcsv.h \
            $${COMMON_SRC}/formats/read_csv.h \
            $${COMMON_SRC}/formats/read_matlab4.h \
            $${COMMON_SRC}/formats/write_matlab4.h \
            $${COMMON_SRC}/warnings_off.h \
            $${COMMON_SRC}/warnings_on.h \
            $${COMMON_SRC}/opt_problem.h \
            $${COMMONGUI_SRC}/commonapp.h \
            $${COMMONGUI_SRC}/brushconf.h \
            $${COMMONGUI_SRC}/graphconfig.h \
            $${COMMONGUI_SRC}/penconfig.h \
            $${COMMONGUI_SRC}/seriesconfig.h \
            $${COMMONGUI_SRC}/integrator.h \
            $${COMMONGUI_SRC}/outputs.h \
            $${COMMONGUI_SRC}/plotgraphs.h \
            $${COMMONGUI_SRC}/qtabwidgetext.h \
            $${COMMONGUI_SRC}/videoconf.h

INCLUDEPATH += $${COMMON_SRC} \    # Common
               $${COMMONGUI_SRC} \ # Common_gui
               $${QUAZIP_SRC} \    # Quazip
               $${FMIPP_SRC}       # FMI++

win32-g++{
    INCLUDEPATH += $${SUNDIALS_SRC} # Sundials

    LIBS += -L$${FMIPP_LIB}/ -lfmippim \                             # FMI++
            -L$${FMIPP_LIB}/ -lfmippex \                             # FMI++
            -L$${QUAZIP_LIB}/release/ -lquazip \                     # Quazip
            -L$${SUNDIALS_LIB}/src/cvode/ -lsundials_cvode \         # Sundials
            -L$${SUNDIALS_LIB}/src/nvec_ser/ -lsundials_nvecserial \ # Sundials
}

macos{
    INCLUDEPATH += $${SUNDIALS_SRC} # Sundials

    LIBS += -L$${FMIPP_LIB}/ -lfmippim \                             # FMI++
            -L$${FMIPP_LIB}/ -lfmippex \                             # FMI++
            -L$${QUAZIP_LIB}/quazip/ -lquazip \                      # Quazip
            -L$${SUNDIALS_LIB}/src/cvode/ -lsundials_cvode \         # Sundials
            -L$${SUNDIALS_LIB}/src/nvec_ser/ -lsundials_nvecserial \ # Sundials
}

unix:!macos{
    LIBS += -L$${FMIPP_LIB} -lfmippim \ # FMI++
            -L$${FMIPP_LIB} -lfmippex \ # FMI++
            -ldl \                      # libdl
            -lquazip5 \                 # Quazip
            -lsundials_cvode \          # Sundials
            -lsundials_nvecserial \     # Sundials
            -lopencv_core -lopencv_imgcodecs -lopencv_highgui -lopencv_videoio -lopencv_imgproc # OpenCV
}

macos{
    # FIX: OpenMP support on MacOS
    # FIX: path directly specified
    INCLUDEPATH += /usr/local/Cellar/opencv/3.4.1_2/include
    INCLUDEPATH += /usr/local/Cellar/boost/1.67.0/include
    LIBS += -L/usr/local/Cellar/opencv/3.4.1_2/lib
    LIBS += -lopencv_core -lopencv_imgcodecs -lopencv_highgui -lopencv_videoio -lopencv_imgproc # OpenCV
}

FORMS    += mainwindow.ui \
            svgviewer.ui \
            about.ui \
            simlog.ui \
            resultwin.ui \
            $${COMMONGUI_SRC}/brushconf.ui \
            $${COMMONGUI_SRC}/graphconfig.ui \
            $${COMMONGUI_SRC}/penconfig.ui \
            $${COMMONGUI_SRC}/seriesconfig.ui \
            $${COMMONGUI_SRC}/integrator.ui \
            $${COMMONGUI_SRC}/outputs.ui \
            $${COMMONGUI_SRC}/plotgraphs.ui \
            $${COMMONGUI_SRC}/videoconf.ui

# Binary for Linux
# -----------------------------
QMAKE_RPATHDIR += $${FMIPP_LIB}

# OpenMP support
# --------------
!macos{
    QMAKE_CXXFLAGS += -fopenmp
    QMAKE_LFLAGS   += -fopenmp
}

DISTFILES +=
